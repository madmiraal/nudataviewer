# nuDataViewer #

Cross-platform C++ code for viewing nu saved data files:

## Dependencies ##
This code depends on:

* [wxWidgets](https://www.wxwidgets.org/)
* [MAUtils](https://bitbucket.org/madmiraal/mautils)
* [MAwxUtils](https://bitbucket.org/madmiraal/mawxutils)
* [MAIMU](https://bitbucket.org/madmiraal/maimu)
* [MADSP](https://bitbucket.org/madmiraal/madsp)