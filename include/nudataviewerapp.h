// (c) 2017: Marcel Admiraal

#ifndef NUDATAVIEWERAPP_H
#define NUDATAVIEWERAPP_H

#include <wx/app.h>

namespace nu
{
    class DataViewerApp : public wxApp
    {
    public:
        /**
         * Initialises the application.
         *
         * @return True to continue processing.
         */
        virtual bool OnInit();
    };
}

#endif // NUDATAVIEWERAPP_H
