// (c) 2017: Marcel Admiraal

#ifndef NUDATAVIEWRFRAME_H
#define NUDATAVIEWRFRAME_H

#include <wx/frame.h>
#include <wx/stattext.h>
#include <wx/sizer.h>

#include "str.h"
#include "plot2dpanel.h"
#include "nudata.h"
#include "multifilter.h"

namespace nu
{
    class DataViewerFrame : public wxFrame
    {
    public:
        DataViewerFrame(wxWindow* parent);
        virtual ~DataViewerFrame();
        void setStatusMessage(const ma::Str& message);

    private:
        void load(wxCommandEvent& event);
        void setSampleFrequency(wxCommandEvent& event);
        void loadConfig(wxCommandEvent& event);
        void saveConfig(wxCommandEvent& event);
        void configFilters(wxCommandEvent& event);
        void startChanged(wxCommandEvent& event);
        void analogueBitsChanged(wxCommandEvent& event);
        void onIdle(wxIdleEvent& evt);

        bool loadConfig(const wxString& filename);
        bool saveConfig(const wxString& filename);
        void clearData();
        void initialiseData();
        void clearDisplaySizer();
        void populateDisplaySizer();
        bool loadFile(const wxString& filename);
        void addData(IMUAnalogueData* imuData, unsigned int imu, int index);
        void plotData();
        bool extractPacket(ma::Str line, unsigned char* packet,
                unsigned int packetLength);

        wxStatusBar* statusBar;
        ma::Vector** accelerometerData;
        ma::Vector** gyroscopeData;
        ma::Vector** magnetometerData;
        ma::Vector** analogueData;
        ma::Plot2DPanel** accelerometerPanel;
        ma::Plot2DPanel** gyroscopePanel;
        ma::Plot2DPanel** magnetometerPanel;
        ma::Plot2DPanel** analoguePanel;
        wxStaticText* accelerometerText;
        wxStaticText* gyroscopeText;
        wxStaticText* magnetometerText;
        wxStaticText* analogueText;
        wxStaticText* analogueBitsText;
        wxBoxSizer* displaySizer;
        unsigned int imus;
        ma::Vector points;
        unsigned int analogueBits;
        ma::MultiFilter accelerometerMultiFilter;
        ma::MultiFilter gyroscopeMultiFilter;
        ma::MultiFilter magnetometerMultiFilter;
        ma::MultiFilter analogueMultiFilter;
        bool modifyingFilters;
        unsigned int sampleFrequency;
    };

    enum {
        ID_LOAD_CONFIG,
        ID_SAVE_CONFIG,
        ID_CONFIG_ACCELEROMETER_FILTERS,
        ID_CONFIG_GYROSCOPE_FILTERS,
        ID_CONFIG_MAGNETOMETER_FILTERS,
        ID_CONFIG_ANALOGUE_FILTERS,
        ID_SET_SAMPLE_FREQUENCY,
        ID_TEST,
        ID_SENDIMUDATA,
        ID_SENDQUATERNIONS,
        ID_STOPSENDING,
        ID_CALIBRATE,
        ID_SET_REGISTER,
        ID_SAVE_TO_FLASH,
        ID_FREEZE,
        ID_DISPLAY_CUBE,
        ID_DISPLAY_ACCELEROMETER,
        ID_DISPLAY_MAGNETOMETER
    };
}

#endif // NUDATAVIEWRFRAME_H
