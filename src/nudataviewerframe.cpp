// (c) 2017: Marcel Admiraal

#include "nudataviewerframe.h"

#include <wx/menu.h>
#include <wx/slider.h>
#include <wx/filedlg.h>
#include <wx/msgdlg.h>
#include <wx/textdlg.h>

#include "strstream.h"
#include "nuapi.h"
#include "multifilterpanel.h"

#include <fstream>

#define AXES 3
#define CHANNELS 8

namespace nu
{
    DataViewerFrame::DataViewerFrame(wxWindow* parent) :
        wxFrame(parent, wxID_ANY, wxT("nu Data Viewer"), wxDefaultPosition,
        wxSize(386, 334)),
        accelerometerData(0), gyroscopeData(0), magnetometerData(0),
        analogueData(0), accelerometerPanel(0), gyroscopePanel(0),
        magnetometerPanel(0), analoguePanel(0), imus(1), analogueBits(9),
        accelerometerMultiFilter(AXES), gyroscopeMultiFilter(AXES),
        magnetometerMultiFilter(AXES), analogueMultiFilter(CHANNELS),
        modifyingFilters(false), sampleFrequency(1000)
    {
        // Create menus.
        wxMenuBar* menuBar = new wxMenuBar();

        wxMenu* fileMenu = new wxMenu();
        fileMenu->Append(wxID_FILE, wxT("Load file"));
        Bind(wxEVT_COMMAND_MENU_SELECTED, &DataViewerFrame::load, this, wxID_FILE);
        menuBar->Append(fileMenu, wxT("File"));

        wxMenu* configMenu = new wxMenu();
        configMenu->Append(ID_LOAD_CONFIG, wxT("Load Configuration"));
        Bind(wxEVT_COMMAND_MENU_SELECTED, &DataViewerFrame::loadConfig, this,
                ID_LOAD_CONFIG);
        configMenu->Append(ID_SAVE_CONFIG, wxT("Save Configuration"));
        Bind(wxEVT_COMMAND_MENU_SELECTED, &DataViewerFrame::saveConfig, this,
                ID_SAVE_CONFIG);
        configMenu->Append(ID_CONFIG_ACCELEROMETER_FILTERS,
                wxT("Accelerometer Filters"));
        Bind(wxEVT_COMMAND_MENU_SELECTED, &DataViewerFrame::configFilters, this,
                ID_CONFIG_ACCELEROMETER_FILTERS);
        configMenu->Append(ID_CONFIG_GYROSCOPE_FILTERS,
                wxT("Gyroscope Filters"));
        Bind(wxEVT_COMMAND_MENU_SELECTED, &DataViewerFrame::configFilters, this,
                ID_CONFIG_GYROSCOPE_FILTERS);
        configMenu->Append(ID_CONFIG_MAGNETOMETER_FILTERS,
                wxT("Magnetometer Filters"));
        Bind(wxEVT_COMMAND_MENU_SELECTED, &DataViewerFrame::configFilters, this,
                ID_CONFIG_MAGNETOMETER_FILTERS);
        configMenu->Append(ID_CONFIG_ANALOGUE_FILTERS,
                wxT("Analogue Filters"));
        Bind(wxEVT_COMMAND_MENU_SELECTED, &DataViewerFrame::configFilters, this,
                ID_CONFIG_ANALOGUE_FILTERS);
        configMenu->Append(ID_SET_SAMPLE_FREQUENCY, wxT("Set Sample Frequency"));
        Bind(wxEVT_COMMAND_MENU_SELECTED, &DataViewerFrame::setSampleFrequency, this,
                ID_SET_SAMPLE_FREQUENCY);
        menuBar->Append(configMenu, wxT("Configuration"));

        SetMenuBar(menuBar);

        statusBar = CreateStatusBar();

        wxSizer* frameSizer = new wxBoxSizer(wxVERTICAL);
        wxSlider* startSlider = new wxSlider(this, wxID_ANY, 0, 0, 1000,
                wxDefaultPosition, wxSize(100, 10));
        startSlider->Bind(wxEVT_SLIDER, &DataViewerFrame::startChanged, this);
        frameSizer->Add(startSlider, 0, wxEXPAND);

        displaySizer = new wxBoxSizer(wxVERTICAL);
        populateDisplaySizer();
        frameSizer->Add(displaySizer, 1, wxEXPAND);

        SetSizer(frameSizer);

        loadConfig("configuration.dat");

        Bind(wxEVT_IDLE, &DataViewerFrame::onIdle, this);
    }

    DataViewerFrame::~DataViewerFrame()
    {
        clearData();
    }

    void DataViewerFrame::setStatusMessage(const ma::Str& message)
    {
        wxString statusMessage(message.c_str());
        statusBar->GetEventHandler()->CallAfter(
                &wxStatusBar::SetStatusText, statusMessage, 0);
    }

    void DataViewerFrame::load(wxCommandEvent& event)
    {
        wxFileDialog loadFileDialog(this, wxT("Load Data File"),
                wxEmptyString, wxEmptyString, wxFileSelectorDefaultWildcardStr,
                wxFD_OPEN|wxFD_FILE_MUST_EXIST);
        if (loadFileDialog.ShowModal() == wxID_CANCEL) return;
        if (loadFile(loadFileDialog.GetPath()))
            plotData();
        else
            wxMessageBox(wxT("Load file failed."));
    }

    void DataViewerFrame::setSampleFrequency(wxCommandEvent& event)
    {
        bool valid = false;
        long longValue;
        wxString frequencyString;
        frequencyString << sampleFrequency;
        while (!valid)
        {
            wxTextEntryDialog* entryDialog = new wxTextEntryDialog(this,
                    wxT("Sample Frequency (Hz):"), wxT("Set Sample Frequency"),
                    frequencyString);
            if (entryDialog->ShowModal() == wxID_CANCEL) return;
            if (!entryDialog->GetValue().ToLong(&longValue)
                || longValue <= 0 || longValue > 1000)
            {
                wxMessageDialog* messageDialog = new wxMessageDialog(this,
                        wxT("Must be a positive integer less than 1000."),
                        wxT("Invalid Entry"));
                messageDialog->ShowModal();
            }
            else
            {
                valid = true;
            }
        }
        sampleFrequency = longValue;

        clearDisplaySizer();
        populateDisplaySizer();
        Layout();
        plotData();
    }

    void DataViewerFrame::loadConfig(wxCommandEvent& event)
    {
        wxFileDialog loadFileDialog(this, wxT("Load Configuration"),
                wxEmptyString, wxT("configuration.dat"),
                wxFileSelectorDefaultWildcardStr,
                wxFD_OPEN|wxFD_FILE_MUST_EXIST);
        if (loadFileDialog.ShowModal() == wxID_CANCEL) return;
        if (!loadConfig(loadFileDialog.GetPath()))
            wxMessageBox(wxT("Load Failed"));
    }

    void DataViewerFrame::saveConfig(wxCommandEvent& event)
    {
        wxFileDialog saveFileDialog(this, wxT("Save Configuration"),
                wxEmptyString, wxT("configuration.dat"),
                wxFileSelectorDefaultWildcardStr,
                wxFD_SAVE|wxFD_OVERWRITE_PROMPT);
        if (saveFileDialog.ShowModal() == wxID_CANCEL) return;
        if (!saveConfig(saveFileDialog.GetPath()))
            wxMessageBox(wxT("Save Failed"));
    }

    void DataViewerFrame::configFilters(wxCommandEvent& event)
    {
        modifyingFilters = true;
        ma::MultiFilter filterCopy;
        ma::MultiFilter* filterModify;
        switch (event.GetId())
        {
            case ID_CONFIG_ACCELEROMETER_FILTERS:
                filterCopy = accelerometerMultiFilter;
                filterModify = &accelerometerMultiFilter;
                break;
            case ID_CONFIG_GYROSCOPE_FILTERS:
                filterCopy = gyroscopeMultiFilter;
                filterModify = &gyroscopeMultiFilter;
                break;
            case ID_CONFIG_MAGNETOMETER_FILTERS:
                filterCopy = magnetometerMultiFilter;
                filterModify = &magnetometerMultiFilter;
                break;
            case ID_CONFIG_ANALOGUE_FILTERS:
                filterCopy = analogueMultiFilter;
                filterModify = &analogueMultiFilter;
                break;
        }
        ma::MultiFilterDialog filterDialog(filterModify, this);
        if (filterDialog.ShowModal() == wxID_CANCEL)
        {
            switch (event.GetId())
            {
                case ID_CONFIG_ACCELEROMETER_FILTERS:
                    accelerometerMultiFilter = filterCopy;
                    break;
                case ID_CONFIG_GYROSCOPE_FILTERS:
                    gyroscopeMultiFilter = filterCopy;
                    break;
                case ID_CONFIG_MAGNETOMETER_FILTERS:
                    magnetometerMultiFilter = filterCopy;
                    break;
                case ID_CONFIG_ANALOGUE_FILTERS:
                    analogueMultiFilter = filterCopy;
                    break;
            }
        }
        plotData();
        modifyingFilters = false;
    }

    void DataViewerFrame::startChanged(wxCommandEvent& event)
    {
        double percent = event.GetInt() / 1000.0;
        for (unsigned int imu = 0; imu < imus; ++imu)
        {
            double xStart = percent * (points[imu] - sampleFrequency);
            double xFinish = xStart + sampleFrequency;
            accelerometerPanel[imu]->setXRange(xStart, xFinish);
            gyroscopePanel[imu]->setXRange(xStart, xFinish);
            magnetometerPanel[imu]->setXRange(xStart, xFinish);
            analoguePanel[imu]->setXRange(xStart, xFinish);
        }
    }

    void DataViewerFrame::analogueBitsChanged(wxCommandEvent& event)
    {
        analogueBits = event.GetInt();
        wxString scaleString = wxT("Bits: (");
        scaleString << analogueBits << ")";
        analogueBitsText->GetEventHandler()->CallAfter(
                &wxStaticText::SetLabel, scaleString);
        double yMax = 1.1 / (1 << (15 - analogueBits));
        for (unsigned int imu = 0; imu < imus; ++imu)
        {
            analoguePanel[imu]->setYRange(-yMax, yMax);
        }
    }

    void DataViewerFrame::onIdle(wxIdleEvent& evt)
    {
        if (modifyingFilters) plotData();
    }

    bool DataViewerFrame::loadConfig(const wxString& filename)
    {
        std::ifstream fileStream(filename.c_str(), std::ifstream::in);
        if (!fileStream.is_open()) return false;

        ma::MultiFilter savedAccelerometerMultiFilter;
        ma::MultiFilter savedGyroscopeMultiFilter;
        ma::MultiFilter savedMagnetometerMultiFilter;
        ma::MultiFilter savedAnalogueMultiFilter;
        unsigned int savedSampleFrequency;

        fileStream >> savedAccelerometerMultiFilter;
        fileStream >> savedGyroscopeMultiFilter;
        fileStream >> savedMagnetometerMultiFilter;
        fileStream >> savedAnalogueMultiFilter;
        fileStream >> savedSampleFrequency;

        if (fileStream.good() &&
                savedAccelerometerMultiFilter.getChannels() == AXES &&
                savedGyroscopeMultiFilter.getChannels() == AXES &&
                savedMagnetometerMultiFilter.getChannels() == AXES &&
                analogueMultiFilter.getChannels() == CHANNELS)
        {
            accelerometerMultiFilter = savedAccelerometerMultiFilter;
            gyroscopeMultiFilter = savedGyroscopeMultiFilter;
            magnetometerMultiFilter = savedMagnetometerMultiFilter;
            analogueMultiFilter = savedAnalogueMultiFilter;
            sampleFrequency = savedSampleFrequency;

            return true;
        }
        else
        {
            return false;
        }
    }

    bool DataViewerFrame::saveConfig(const wxString& filename)
    {
        std::ofstream fileStream(filename.c_str(), std::ofstream::out);
        if (!fileStream.is_open()) return false;

        fileStream << accelerometerMultiFilter << '\t';
        fileStream << gyroscopeMultiFilter << '\t';
        fileStream << magnetometerMultiFilter << '\t';
        fileStream << analogueMultiFilter << '\t';
        fileStream << sampleFrequency << std::endl;

        return true;
    }

    void DataViewerFrame::clearData()
    {
        for (unsigned int imu = 0; imu < imus; ++imu)
        {
            if (accelerometerData != 0 && accelerometerData[imu] != 0)
            {
                delete[] accelerometerData[imu];
                accelerometerData[imu] = 0;
            }
            if (gyroscopeData != 0 && gyroscopeData[imu] != 0)
            {
                delete[] gyroscopeData[imu];
                gyroscopeData[imu] = 0;
            }
            if (magnetometerData !=0 && magnetometerData[imu] != 0)
            {
                delete[] magnetometerData[imu];
                magnetometerData[imu] = 0;
            }
            if (analogueData != 0 && analogueData[imu] != 0)
            {
                delete[] analogueData[imu];
                analogueData[imu] = 0;
            }
        }
        if (accelerometerData != 0)
        {
            delete[] accelerometerData;
            accelerometerData = 0;
        }
        if (gyroscopeData != 0)
        {
            delete[] gyroscopeData;
            gyroscopeData = 0;
        }
        if (magnetometerData != 0)
        {
            delete[] magnetometerData;
            magnetometerData = 0;
        }
        if (analogueData != 0)
        {
            delete[] analogueData;
            analogueData = 0;
        }
    }

    void DataViewerFrame::initialiseData()
    {
        accelerometerData = new ma::Vector*[imus]();
        gyroscopeData = new ma::Vector*[imus]();
        magnetometerData = new ma::Vector*[imus]();
        analogueData = new ma::Vector*[imus]();
        for (unsigned int imu = 0; imu < imus; ++imu)
        {
            accelerometerData[imu] = new ma::Vector[AXES];
            gyroscopeData[imu] = new ma::Vector[AXES];
            magnetometerData[imu] = new ma::Vector[AXES];
            analogueData[imu] = new ma::Vector[CHANNELS];
            for (unsigned int axis = 0; axis < AXES; ++axis)
            {
                accelerometerData[imu][axis] = ma::Vector(points[imu]);
                gyroscopeData[imu][axis] = ma::Vector(points[imu]);
                magnetometerData[imu][axis] = ma::Vector(points[imu]);
            }
            for (unsigned int channel = 0; channel < CHANNELS; ++channel)
            {
                analogueData[imu][channel] = ma::Vector(points[imu]);
            }
        }
    }

    void DataViewerFrame::clearDisplaySizer()
    {
        displaySizer->Clear(true);
        if (accelerometerPanel != 0)
        {
            delete[] accelerometerPanel;
            accelerometerPanel = 0;
        }
        if (gyroscopePanel != 0)
        {
            delete[] gyroscopePanel;
            gyroscopePanel = 0;
        }
        if (magnetometerPanel != 0)
        {
            delete[] magnetometerPanel;
            magnetometerPanel = 0;
        }
        if (analoguePanel != 0)
        {
            delete[] analoguePanel;
            analoguePanel = 0;
        }
    }

    void DataViewerFrame::populateDisplaySizer()
    {
        accelerometerText = new wxStaticText(this, wxID_ANY,
                wxT("Accelerometer"));
        displaySizer->Add(accelerometerText);
        accelerometerPanel = new ma::Plot2DPanel*[imus];
        for (unsigned int imu = 0; imu < imus; ++imu)
        {
            accelerometerPanel[imu] = new ma::Plot2DPanel(
                    this, AXES, 0, sampleFrequency, -1, 1);
            displaySizer->Add(accelerometerPanel[imu], 1, wxEXPAND);
        }
        gyroscopeText = new wxStaticText(this, wxID_ANY,
                wxT("Gyroscope"));
        displaySizer->Add(gyroscopeText);
        gyroscopePanel = new ma::Plot2DPanel*[imus];
        for (unsigned int imu = 0; imu < imus; ++imu)
        {
            gyroscopePanel[imu] = new ma::Plot2DPanel(
                    this, AXES, 0, sampleFrequency, -1, 1);
            displaySizer->Add(gyroscopePanel[imu], 1, wxEXPAND);
        }
        magnetometerText = new wxStaticText(this, wxID_ANY,
                wxT("Magnetometer"));
        displaySizer->Add(magnetometerText);
        magnetometerPanel = new ma::Plot2DPanel*[imus];
        for (unsigned int imu = 0; imu < imus; ++imu)
        {
            magnetometerPanel[imu] = new ma::Plot2DPanel(
                    this, AXES, 0, sampleFrequency, -1, 1);
            displaySizer->Add(magnetometerPanel[imu], 1, wxEXPAND);
        }
        wxSizer* analogueSizer = new wxBoxSizer(wxHORIZONTAL);
        analogueText = new wxStaticText(this, wxID_ANY,
                wxT("Analogue"));
        analogueSizer->Add(analogueText, 1, wxEXPAND);
        wxString scaleString = wxT("Bits: (");
        scaleString << analogueBits << ")";
        analogueBitsText = new wxStaticText(this, wxID_ANY, scaleString,
                wxDefaultPosition, wxSize(60,1));
        analogueSizer->Add(analogueBitsText, 0, wxEXPAND);
        wxSlider* analogueBitsSlider = new wxSlider(
                this, wxID_ANY, analogueBits, 0, 15,
                wxDefaultPosition, wxSize(50, 1));
        analogueBitsSlider->Bind(wxEVT_SLIDER, &DataViewerFrame::analogueBitsChanged,
                this);
        analogueSizer->Add(analogueBitsSlider, 1, wxEXPAND);
        displaySizer->Add(analogueSizer, 0, wxEXPAND);
        double yMax = 1.1 / (1 << (15 - analogueBits));
        analoguePanel = new ma::Plot2DPanel*[imus];
        for (unsigned int imu = 0; imu < imus; ++imu)
        {
            analoguePanel[imu] = new ma::Plot2DPanel(
                    this, CHANNELS, 0, sampleFrequency, -yMax, yMax);
            displaySizer->Add(analoguePanel[imu], 1, wxEXPAND);
        }
    }

    bool DataViewerFrame::loadFile(const wxString& filename)
    {
        std::ifstream fileStream(filename.c_str(), std::ifstream::in);
        if (!fileStream.is_open()) return false;
        unsigned char packet[36];
        ma::Str line;

        // Count points.
        unsigned int newImus = 0;
        ma::Vector newPoints;
        while(ma::getNext(fileStream, line))
        {
            if (!extractPacket(line, packet, 36)) continue;
            unsigned int imu = (unsigned int)packet[0];
            if (imu >= newImus)
            {
                newPoints.extend(imu - newImus + 1);
                newImus = imu + 1;
            }
            ++newPoints[imu];
        }
        if (newImus == 0) return false;

        // Confirm IMU count.
        long longValue = 0;
        bool valid = false;
        while (!valid)
        {
            ma::StrStream imusText("IMU points found: ");
            newPoints.print(imusText);
            imusText << "\nUse first: ";
            wxString imuCountText; imuCountText << newImus;
            wxTextEntryDialog* confirmDialog = new wxTextEntryDialog(this,
                    wxString(imusText.toStr().c_str()),
                    wxT("Confirm IMU Count"), imuCountText);
            if (confirmDialog->ShowModal() == wxID_CANCEL ||
                !confirmDialog->GetValue().ToLong(&longValue)
                || longValue <= 0 || longValue > newImus)
            {
                wxString messageText("Must be a positive integer less than ");
                messageText << newImus << ".";
                wxMessageDialog* messageDialog = new wxMessageDialog(this,
                        messageText, wxT("Invalid Entry"));
                messageDialog->ShowModal();
            }
            else
            {
                valid = true;
            }
        }
        newImus = longValue;
        newPoints = newPoints.subVector(0, newImus);

        // Clear old data.
        clearDisplaySizer();
        clearData();

        // Initialise new data.
        imus = newImus;
        points = newPoints;
        initialiseData();
        populateDisplaySizer();
        Layout();

        // Go back to the beginning of the file.
        fileStream.clear();
        fileStream.seekg(0, std::ios::beg);

        points = ma::Vector(imus);
        unsigned int badLines = 0;
        while(ma::getNext(fileStream, line))
        {
            if (!extractPacket(line, packet, 36))
            {
                ++badLines;
                continue;
            }
            ma::Data* data = extractIMUData(&packet[1], 34);
            nu::IMUAnalogueData* imuData = dynamic_cast<nu::IMUAnalogueData*>(data);
            if (!imuData) continue;
            unsigned int imu = (unsigned int)packet[0];
            if (imu < imus)
            {
                addData(imuData, imu, points[imu]);
                ++points[imu];
            }
            else
            {
                ++badLines;
                continue;
            }
            delete data;
        }
        ma::StrStream statusMessage("Loaded ");
        points.print(statusMessage);
        statusMessage << " points. Skipped " << badLines << " lines.";
        setStatusMessage(statusMessage.toStr());
        return true;
    }

    void DataViewerFrame::addData(IMUAnalogueData* imuData, unsigned int imu, int index)
    {
        ma::Vector thisAccelerometerData = imuData->getAccelerometerData();
        ma::Vector thisGyroscopeData = imuData->getGyroscopeData();
        ma::Vector thisMagnetometerData = imuData->getMagnetometerData();
        ma::Vector thisAnalogueData = imuData->getAnalogueData();
        for (unsigned int axis = 0; axis < AXES; ++axis)
        {
            accelerometerData[imu][axis][index] = thisAccelerometerData[axis];
            gyroscopeData[imu][axis][index] = thisGyroscopeData[axis];
            magnetometerData[imu][axis][index] = thisMagnetometerData[axis];
        }
        for (unsigned int channel = 0; channel < CHANNELS; ++channel)
        {
            analogueData[imu][channel][index] = thisAnalogueData[channel];
        }
    }

    void DataViewerFrame::plotData()
    {
        for (unsigned int imu = 0; imu < imus; ++imu)
        {
            for (unsigned int axis = 0; axis < AXES; ++axis)
            {
                if (accelerometerData != 0)
                    accelerometerPanel[imu]->setYData(
                            accelerometerMultiFilter.filterStatic(
                            accelerometerData[imu][axis]), axis);
                if (gyroscopeData != 0)
                    gyroscopePanel[imu]->setYData(
                            gyroscopeMultiFilter.filterStatic(
                            gyroscopeData[imu][axis]), axis);
                if (magnetometerData != 0)
                    magnetometerPanel[imu]->setYData(
                            magnetometerMultiFilter.filterStatic(
                            magnetometerData[imu][axis]), axis);
            }
            for (unsigned int channel = 0; channel < CHANNELS; ++channel)
            {
                if (analogueData != 0)
                    analoguePanel[imu]->setYData(
                            analogueMultiFilter.filterStatic(
                            analogueData[imu][channel]), channel);
            }
        }
    }

    bool DataViewerFrame::extractPacket(ma::Str line, unsigned char* packet,
            unsigned int packetLength)
    {
        if (line.length() == 0) return false;
        ma::StrStream lineStream(line);
        unsigned int index = 0;
        while (lineStream.good() && index < 36)
        {
            ma::Str value = ma::getNext(lineStream, ',');
            long number;
            if (ma::integerToLong(value, number))
            {
                packet[index] = (unsigned char)number;
                ++index;
            }
        }
        if (index <= 34)
        {
//            std::cout << "Invalid line [" << index << "]: " << line << std::endl;
            return false;
        }
        return true;
    }
}
