// (c) 2017: Marcel Admiraal

#include "nudataviewerapp.h"

#include "nudataviewerframe.h"

IMPLEMENT_APP(nu::DataViewerApp);

namespace nu
{
    bool DataViewerApp::OnInit()
    {
        DataViewerFrame* frame = new DataViewerFrame(0L);
        frame->Show();
        frame->Maximize();
        return true;
    }
}
